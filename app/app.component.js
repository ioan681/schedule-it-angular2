"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
require('./rxjs-operators');
var AppComponent = (function () {
    function AppComponent() {
    }
    AppComponent.prototype.ngOnInit = function () {
        //google button 
        var meta = document.createElement('meta');
        meta.name = 'google-signin-client_id';
        meta.content = '964204965770-k01hrq2dpmi18ou5lq1ca6b7ktfa70gu.apps.googleusercontent.com';
        document.getElementsByTagName('head')[0].appendChild(meta);
        var node = document.createElement('script');
        node.src = 'https://apis.google.com/js/platform.js?onload=onLoadGoogleAPI';
        node.type = 'text/javascript';
        document.getElementsByTagName('body')[0].appendChild(node);
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            template: "<router-outlet></router-outlet>",
            styleUrls: ['css/week.component.css', 'css/login.component.css']
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var user_service_1 = require('./user.service');
var UserSearchComponent = (function () {
    function UserSearchComponent(UserService, router) {
        this.UserService = UserService;
        this.router = router;
    }
    UserSearchComponent.prototype.ngOnInit = function () {
    };
    UserSearchComponent.prototype.Click = function () {
        var _this = this;
        if (this.result == undefined)
            this.UserService.getUsers().subscribe(function (result) { _this.result = JSON.parse(result["_body"]); });
    };
    UserSearchComponent.prototype.search = function (value) {
        if (value != "") {
            if (this.result != null) {
                var arr_1 = [];
                this.result.forEach(function (element) {
                    if (element["name"].search(value) >= 0) {
                        arr_1.push(element);
                    }
                });
                this.users = arr_1;
            }
        }
        else
            this.users = null;
    };
    UserSearchComponent.prototype.gotoUser = function (user) {
        this.users = null;
        localStorage.setItem('selectedUser', JSON.stringify({ id: user.id, name: user.name, email: user.email, telephone: user.telephone,
            profession: user.profession, about: user.about, img_name: user.img_name }));
        var link = ['/user', user.id];
        this.router.navigate(link);
    };
    UserSearchComponent = __decorate([
        core_1.Component({
            selector: 'user-search',
            templateUrl: 'view/user/user-search.component.html',
        }), 
        __metadata('design:paramtypes', [user_service_1.UserService, router_1.Router])
    ], UserSearchComponent);
    return UserSearchComponent;
}());
exports.UserSearchComponent = UserSearchComponent;
//# sourceMappingURL=user-search.component.js.map
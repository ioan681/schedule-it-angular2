"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var user_service_1 = require('./user.service');
var group_service_1 = require('./group.service');
var group_1 = require('../models/group');
var router_1 = require('@angular/router');
require('../rxjs-operators');
var UserComponent = (function () {
    function UserComponent(service, groupService, router, route, elementRef, renderer) {
        var _this = this;
        this.service = service;
        this.groupService = groupService;
        this.router = router;
        this.route = route;
        this.group = new group_1.Group;
        this.globalListenFunc = renderer.listenGlobal('document', 'click', function (event) {
            var id;
            _this.route.params.forEach(function (params) {
                id = +params['id'];
            });
            if (id != _this.model.id)
                _this.service.getUser(id).subscribe(function (result) {
                    _this.model = JSON.parse(result["_body"]);
                });
        });
    }
    UserComponent.prototype.ngOnInit = function () {
        this.model = JSON.parse(localStorage.getItem('selectedUser'));
    };
    UserComponent.prototype.newGroup = function () {
        var group = document.getElementById('new-group');
        var btn = document.getElementById('new-btn');
        if (group.className == "new-group closed") {
            group.className = "new-group";
            btn.className = "btn-primary btn";
        }
        else {
            group.className = "new-group closed";
            btn.className = "btn-success btn";
        }
    };
    UserComponent.prototype.Create = function () {
        this.groupService.createGroup(this.model.id, this.group.name, this.group.description).subscribe();
    };
    UserComponent.prototype.Groups = function () {
        var _this = this;
        this.groupService.getUserGroups(this.model.id).subscribe(function (result) {
            var arr = JSON.parse(result["_body"]);
            _this.groups = arr[0];
            _this.userGroups = arr[1];
        });
    };
    UserComponent.prototype.Check = function (group) {
        var check = false;
        if (this.userGroups != undefined)
            this.userGroups.forEach(function (element) {
                if (group['id'] == element['id']) {
                    check = true;
                }
            });
        return check;
    };
    UserComponent.prototype.Add = function (id) {
        var _this = this;
        this.groupService.addtoGroup(this.model.id, id).subscribe(function (result) {
            _this.userGroups.push(JSON.parse(result["_body"]));
        });
    };
    UserComponent = __decorate([
        core_1.Component({
            templateUrl: 'view/user/user.component.html',
            styleUrls: ['css/user.component.css'],
        }), 
        __metadata('design:paramtypes', [user_service_1.UserService, group_service_1.GroupService, router_1.Router, router_1.ActivatedRoute, core_1.ElementRef, core_1.Renderer])
    ], UserComponent);
    return UserComponent;
}());
exports.UserComponent = UserComponent;
//# sourceMappingURL=user.component.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var GroupService = (function () {
    function GroupService(http) {
        this.http = http;
        // set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }
    GroupService.prototype.createGroup = function (id, name, description) {
        var headers = new http_1.Headers({ 'Authorization': 'Bearer ' + this.token });
        headers.append('Content-Type', 'application/json,multipart/form-data');
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post('http://schedule.mvm.bg/createGroup', JSON.stringify({ id: id, name: name, description: description }), options)
            .map(function (response) {
            return response;
        });
    };
    GroupService.prototype.getUserGroups = function (id) {
        var headers = new http_1.Headers({ 'Authorization': 'Bearer ' + this.token });
        headers.append('Content-Type', 'application/json');
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.get('http://schedule.mvm.bg/groups/' + id, options)
            .map(function (response) {
            return response;
        });
    };
    GroupService.prototype.addtoGroup = function (user_id, id) {
        var headers = new http_1.Headers({ 'Authorization': 'Bearer ' + this.token });
        headers.append('Content-Type', 'application/json');
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post('http://schedule.mvm.bg/addToGroup', JSON.stringify({ user_id: user_id, id: id }), options)
            .map(function (response) {
            return response;
        });
    };
    GroupService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], GroupService);
    return GroupService;
}());
exports.GroupService = GroupService;
//# sourceMappingURL=group.service.js.map
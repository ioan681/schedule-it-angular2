"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var user_service_1 = require('./user.service');
var ng2_file_upload_1 = require('ng2-file-upload/ng2-file-upload');
require('../rxjs-operators');
var URL = 'http://schedule.mvm.bg/profile2';
if (JSON.parse(localStorage.getItem('currentUser')) == null) {
    localStorage.setItem('currentUser', JSON.stringify({ token: "da" }));
}
var Profile = "Bearer " + JSON.parse(localStorage.getItem('currentUser')).token;
var ProfileComponent = (function () {
    function ProfileComponent(service) {
        this.service = service;
        this.upload = false;
        this.passCheck = true;
        this.model = {};
        this.uploader = new ng2_file_upload_1.FileUploader({ url: URL, authToken: Profile });
    }
    ProfileComponent.prototype.ngOnInit = function () {
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.model.username = currentUser.name;
        this.model.email = currentUser.email;
        this.model.telephone = currentUser.telephone;
        this.model.profession = currentUser.profession;
        this.model.about = currentUser.about;
        this.token = currentUser.token;
        this.image = currentUser.img_name;
    };
    ProfileComponent.prototype.onKey = function (value) {
        this.passCheck = (this.model.pass == value);
    };
    ProfileComponent.prototype.Image = function (event) {
        if (event.srcElement.files[0]) {
            this.model.file = event.srcElement.files[0];
            this.upload = true;
            document.getElementById("picture-class").className = "btn btn-success btn-file";
            this.save = document.getElementById('user_img');
            this.save = this.save.src;
            var reader = new FileReader();
            reader.onload = function () {
                var output = document.getElementById('user_img');
                var output2 = document.getElementById('profile-pic');
                output.src = reader.result;
                output2.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        }
        else {
            this.upload = false;
            document.getElementById("picture-class").className = "btn btn-primary btn-file";
        }
    };
    ProfileComponent.prototype.Update = function () {
        var _this = this;
        this.service.update(this.model)
            .subscribe(function (result) {
            localStorage.removeItem('currentUser');
            localStorage.setItem('currentUser', JSON.stringify({ name: _this.model.username, email: _this.model.email, telephone: _this.model.telephone,
                profession: _this.model.profession, about: _this.model.about, token: _this.token }));
        });
    };
    ProfileComponent.prototype.Close = function (event) {
        var _this = this;
        if (event.srcElement.value != "1") {
            setTimeout(function () {
                if (_this.upload == true && document.getElementById("myModal").className == "modal fade") {
                    var test = document.getElementById('user_img');
                    test.src = _this.save;
                    test = document.getElementById('profile-pic');
                    test.src = _this.save;
                }
            }, 100);
        }
    };
    ProfileComponent = __decorate([
        core_1.Component({
            templateUrl: 'view/user/profile.component.html',
            styleUrls: ['css/user.component.css'],
        }), 
        __metadata('design:paramtypes', [user_service_1.UserService])
    ], ProfileComponent);
    return ProfileComponent;
}());
exports.ProfileComponent = ProfileComponent;
//# sourceMappingURL=profile.component.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var forms_1 = require('@angular/forms');
var user_routing_module_1 = require('./user-routing.module');
var ng2_file_upload_1 = require('ng2-file-upload');
var profile_component_1 = require('./profile.component');
var user_component_1 = require('./user.component');
var group_service_1 = require('./group.service');
var user_service_1 = require('./user.service');
var UserModule = (function () {
    function UserModule() {
    }
    UserModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                ng2_file_upload_1.FileUploadModule,
                forms_1.FormsModule,
                user_routing_module_1.UserRoutingModule,
            ],
            declarations: [
                profile_component_1.ProfileComponent,
                user_component_1.UserComponent
            ],
            providers: [
                user_service_1.UserService,
                group_service_1.GroupService
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], UserModule);
    return UserModule;
}());
exports.UserModule = UserModule;
//# sourceMappingURL=user.module.js.map
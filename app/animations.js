"use strict";
var core_1 = require('@angular/core');
// Component transition animations
exports.slideAside = core_1.trigger('flyInOut', [
    // state('in', style({transform: 'translateX(0)'})),
    core_1.state('prev', core_1.style({
        transform: 'translateX(-2000px)',
    })),
    core_1.state('next', core_1.style({
        transform: 'translateX(2000px)',
    })),
    core_1.transition('next => right', [
        core_1.style({ transform: 'translateX(-500px)' }),
        core_1.animate(250)
    ]),
    core_1.transition('prev => left', [
        core_1.style({ transform: 'translateX(500px)' }),
        core_1.animate(250)
    ]),
    core_1.transition('* => prev', core_1.animate('350ms ease-in')),
    core_1.transition('* => next', core_1.animate('350ms ease-in')),
]);
//# sourceMappingURL=animations.js.map
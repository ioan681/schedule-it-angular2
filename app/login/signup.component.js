"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var auth_service_1 = require('./auth.service');
var SignUpComponent = (function () {
    function SignUpComponent(router, auth) {
        this.router = router;
        this.auth = auth;
        this.model = {};
        this.loading = false;
        this.passCheck = true;
        this.error = '';
    }
    SignUpComponent.prototype.onKey = function (value) {
        this.passCheck = (this.model.pass == value);
    };
    SignUpComponent.prototype.getUser = function () {
        this.auth.getUser().subscribe(function (result) {
            console.log(result);
        });
    };
    SignUpComponent.prototype.ngOnInit = function () {
        // reset login status
        this.auth.logout();
        // document.getElementById('login').appendChild(document.getElementById('my-signin3'))
        // document.getElementById('my-signin3').style.display="inline";
    };
    SignUpComponent.prototype.signUp = function () {
        var _this = this;
        this.loading = true;
        this.auth.signUp(this.model.username, this.model.email, this.model.pass)
            .subscribe(function (result) {
            if (result === true) {
                _this.router.navigate(['/']);
            }
            else {
                _this.error = 'Username or password is incorrect';
                _this.loading = false;
            }
        }, function (error) {
            _this.error = 'Username or password is incorrect';
            _this.loading = false;
        });
    };
    SignUpComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            templateUrl: '../../view/signup.component.html',
            styleUrls: ['../../css/login.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router, auth_service_1.AuthService])
    ], SignUpComponent);
    return SignUpComponent;
}());
exports.SignUpComponent = SignUpComponent;
//# sourceMappingURL=signup.component.js.map
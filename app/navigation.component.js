"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var auth_service_1 = require('./login/auth.service');
require('./rxjs-operators');
var NavigationComponent = (function () {
    function NavigationComponent(auth) {
        this.auth = auth;
        this.isChecked = false;
        this.isMapChecked = true;
        this.isChanged = false;
    }
    ;
    ;
    ;
    NavigationComponent.prototype.ngOnInit = function () {
        var _this = this;
        var d = new Date();
        this.day = d.getMonth() + 1;
        var user;
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        var token = currentUser && currentUser.token;
        this.auth.getUser().subscribe(function (result) {
            user = result.json();
            localStorage.removeItem('currentUser');
            localStorage.setItem('currentUser', JSON.stringify({ name: user.name, email: user.email, telephone: user.telephone,
                profession: user.profession, about: user.about, img_name: user.img_name,
                token: token }));
            document.getElementById("profile-name").innerHTML = user.name;
            document.getElementById("side-name").innerHTML = user.name;
            document.getElementById("side-mail").innerHTML = "Mail: " + user.email;
            _this.image = user.img_name;
        });
    };
    NavigationComponent.prototype.Click = function () {
        this.isChecked = !this.isChecked;
    };
    NavigationComponent.prototype.da = function () {
        var calendar = document.getElementById('calendar-content');
        this.isMapChecked = !this.isMapChecked;
        calendar.style.marginRight = (this.isMapChecked ? '' : '45%');
        var wrapper = document.getElementById('wrapper');
        var map, clas;
        clas = (this.isChecked ? '' : 'toggled');
        map = (this.isMapChecked ? '' : ' mapToggled');
        wrapper.className = clas + map;
        wrapper.setAttribute('ng-reflect-ng-class', wrapper.className);
    };
    NavigationComponent.prototype.Change = function () {
        this.isChanged = !this.isChanged;
    };
    NavigationComponent = __decorate([
        core_1.Component({
            templateUrl: 'view/navigation.component.html',
            styleUrls: ['css/week.component.css', 'css/login.component.css', 'css/month.component.css', 'css/navigation.component.css'],
            outputs: ['month'],
        }), 
        __metadata('design:paramtypes', [auth_service_1.AuthService])
    ], NavigationComponent);
    return NavigationComponent;
}());
exports.NavigationComponent = NavigationComponent;
//# sourceMappingURL=navigation.component.js.map
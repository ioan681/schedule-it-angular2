"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var auth_guard_1 = require('../login/auth.guard');
var year_component_1 = require('./year.component');
var month_component_1 = require('./month.component');
var week_component_1 = require('./week.component');
var navigation_component_1 = require('../navigation.component');
var feed_component_1 = require('./feed.component');
var CalendarRoutes = [
    { path: 'schedule',
        component: navigation_component_1.NavigationComponent,
        canActivate: [auth_guard_1.AuthGuard],
        children: [
            { path: 'year', component: year_component_1.YearComponent },
            { path: 'month/:id', component: month_component_1.MonthComponent },
            { path: 'week', component: week_component_1.WeekComponent },
            { path: '', component: feed_component_1.FeedComponent },
            { path: 'feed', component: feed_component_1.FeedComponent }
        ]
    },
];
var CalendarRoutingModule = (function () {
    function CalendarRoutingModule() {
    }
    CalendarRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forChild(CalendarRoutes)
            ],
            exports: [
                router_1.RouterModule
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], CalendarRoutingModule);
    return CalendarRoutingModule;
}());
exports.CalendarRoutingModule = CalendarRoutingModule;
//# sourceMappingURL=calendar-routing.module.js.map
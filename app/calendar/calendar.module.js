"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var forms_1 = require('@angular/forms');
var core_2 = require('angular2-google-maps/core');
var map_component_1 = require('../map.component');
var year_component_1 = require('./year.component');
var month_component_1 = require('./month.component');
var week_component_1 = require('./week.component');
var feed_component_1 = require('./feed.component');
var feed_service_1 = require('./feed.service');
var calendar_service_1 = require('./calendar.service');
var calendar_routing_module_1 = require('./calendar-routing.module');
var CalendarModule = (function () {
    function CalendarModule() {
    }
    CalendarModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                calendar_routing_module_1.CalendarRoutingModule,
                forms_1.FormsModule,
                core_2.AgmCoreModule.forRoot({
                    apiKey: 'AIzaSyBleBTzUcdI09CD7svMvqIjiST5k8yUTyo'
                }),
            ],
            declarations: [
                year_component_1.YearComponent,
                month_component_1.MonthComponent,
                week_component_1.WeekComponent,
                feed_component_1.FeedComponent,
                map_component_1.MapComponent,
                map_component_1.AddMarkerMapComponent,
            ],
            providers: [
                calendar_service_1.CalendarService,
                feed_service_1.FeedService
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], CalendarModule);
    return CalendarModule;
}());
exports.CalendarModule = CalendarModule;
//# sourceMappingURL=calendar.module.js.map
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var animations_1 = require('../animations');
var month_component_1 = require('./month.component');
var WeekComponent = (function (_super) {
    __extends(WeekComponent, _super);
    function WeekComponent() {
        _super.apply(this, arguments);
        this.colourVal = 1;
        this.colour = "#0275d8";
    }
    WeekComponent.prototype.ngOnInit = function () {
        //console.log(document.getElementById("side-container"))
        this.cont2 = document.getElementById("weekContainer2");
        this.attr = this.cont2.attributes[0]["nodeName"];
        this.sideContainer = document.getElementById("side-img");
        this.sideMonth = document.getElementById("month");
        // var side = document.getElementById('side-month-input')
        // side.style.display = "block";
        //document.getElementById('side-month').appendChild(side);
        this.Create(0);
    };
    WeekComponent.prototype.Days = function () {
        // this.route.params.forEach((params: Params) => {
        // 	this.month = +params['id'];
        // 	});
        var mon;
        this.year = (new Date()).getFullYear();
        var d = new Date();
        var day = d.getDay();
        var today = d.getDate();
        this.month = d.getMonth() + 1;
        console.log(today);
        if (today - day < 0) {
            if (this.month == 1) {
                this.month = 12;
                this.year -= 1;
            }
            else {
                this.month -= 1;
            }
            var obj = { mon: 0 };
            this.GetMonth(obj);
            this.lastday = obj.mon + today - day;
        }
        else
            this.lastday = today - day;
        return this.Week();
    };
    WeekComponent.prototype.Week = function () {
        var obj = { mon: 0 };
        this.GetMonth(obj);
        var j = this.lastday;
        var days = new Array;
        if (this.lastday - 7 < 0) {
            if (this.month == 1) {
                this.month = 12;
                this.year -= 1;
            }
            else
                this.month -= 1;
        }
        for (var i = 0; i < 7; i += 1) {
            if (j == obj.mon)
                j = 0;
            j += 1;
            days.push(j);
        }
        this.lastday = j;
        if (days[0] < days[6] && days[0] - 7 <= 0) {
            if (this.month == 12) {
                this.month = 1;
                this.year += 1;
            }
            else
                this.month += 1;
        }
        this.Month();
        if (days[0] > days[6] && days[6] - 7 <= 0) {
            if (this.month == 12) {
                this.month = 1;
                this.year += 1;
            }
            else
                this.month += 1;
        }
        return days;
    };
    WeekComponent.prototype.Week1 = function () {
        var obj = { mon: 0 };
        if (this.lastday - 14 >= 0) {
            this.lastday -= 14;
            this.GetMonth(obj);
        }
        else {
            if (this.month == 1) {
                this.month = 12;
                this.year -= 1;
                this.GetMonth(obj);
            }
            else {
                this.month -= 1;
                this.GetMonth(obj);
            }
            this.lastday = (this.lastday - 14 + obj.mon);
        }
        var days = new Array;
        var j = this.lastday;
        for (var i = 0; i < 7; i += 1) {
            if (j == obj.mon)
                j = 0;
            j += 1;
            days.push(j);
        }
        this.lastday = j;
        if (this.lastday - 7 < 0) {
            if (this.month == 12) {
                this.month = 1;
                this.year += 1;
            }
            else
                this.month += 1;
        }
        this.Month();
        return days;
    };
    WeekComponent.prototype.GetMonth = function (obj) {
        if (this.month <= 7) {
            if (this.month % 2 == 1)
                obj.mon = 31;
            else
                obj.mon = 30;
            if (this.month == 2) {
                if (((this.year % 4 == 0) && (this.year % 100 != 0))
                    || (this.year % 400 == 0))
                    obj.mon = 29;
                else
                    obj.mon = 28;
            }
        }
        else {
            if (this.month % 2 == 1)
                obj.mon = 30;
            else
                obj.mon = 31;
        }
    };
    WeekComponent.prototype.Create = function (n) {
        var _this = this;
        var container = document.getElementById("weekContainer");
        var div = container;
        div.innerHTML = "";
        var table = document.createElement('table');
        var tbody = document.createElement('tbody');
        tbody.className = "year table table-bordered";
        var tr = document.createElement('tr');
        //tr.setAttribute(this.attr, this.attr);
        table.className = "year table table-bordered";
        var td, week;
        switch (n) {
            case -1:
                week = this.Week1();
                break;
            case 0:
                week = this.Days();
                break;
            case 1:
                week = this.Week();
                break;
        }
        week.forEach(function (element) {
            td = _this.hours(element);
            tr.appendChild(td);
        });
        this.showAttach(week[0]);
        tbody.appendChild(tr);
        table.appendChild(tbody);
        table.setAttribute(this.attr, this.attr);
        this.cont2.removeChild(container);
        div.appendChild(table);
        this.cont2.appendChild(div);
    };
    WeekComponent.prototype.Prev = function () {
        var _this = this;
        this.state = "prev";
        this.Create(-1);
        setTimeout(function () {
            _this.state = "left";
        }, 450);
    };
    WeekComponent.prototype.Next = function () {
        var _this = this;
        this.state = "next";
        this.Create(1);
        setTimeout(function () {
            _this.state = "right";
        }, 450);
    };
    WeekComponent.prototype.Month = function () {
        switch (this.month) {
            case 1:
                this.month2 = "January";
                break;
            case 2:
                this.month2 = "February";
                break;
            case 3:
                this.month2 = "March";
                break;
            case 4:
                this.month2 = "April";
                break;
            case 5:
                this.month2 = "May";
                break;
            case 6:
                this.month2 = "June";
                break;
            case 7:
                this.month2 = "July";
                break;
            case 8:
                this.month2 = "August";
                break;
            case 9:
                this.month2 = "September";
                break;
            case 10:
                this.month2 = "October";
                break;
            case 11:
                this.month2 = "November";
                break;
            case 12:
                this.month2 = "December";
                break;
        }
        this.year2 = this.year;
        _super.prototype.SideTable.call(this);
    };
    WeekComponent.prototype.Color = function (val) {
        switch (val) {
            case 1:
                this.colour = "blue";
                this.colourVal = 1;
                break;
            case 2:
                this.colour = "green";
                this.colourVal = 2;
                break;
            case 3:
                this.colour = "red";
                this.colourVal = 3;
                break;
            case 4:
                this.colour = "purple";
                this.colourVal = 4;
                break;
            case 5:
                this.colour = "yellow";
                this.colourVal = 5;
                break;
        }
    };
    WeekComponent.prototype.clicked = function (event) {
        var modal = document.getElementById('myModal');
        var from = document.getElementById('from');
        var to = document.getElementById('to');
        var val = event.target.innerHTML;
        this.target = event.target;
        from.value = val;
        val = val.split(":");
        var val2 = val[1];
        val = (parseInt(val[0]) + 1);
        if (val < 10)
            val = "0" + val;
        val = val + ":" + val2;
        to.value = val;
        setTimeout(function () {
            var map = document.getElementById('map');
            google.maps.event.trigger(map, "resize");
        }, 250);
    };
    WeekComponent.prototype.showAttach = function (day) {
        var date = this.year + "-" + this.month + "-" + day;
        var arr = new Array;
        this.appoint.getAppointment(date).subscribe(function (result) {
            if (result.length > 0) {
                arr = result;
                var d = void 0, parent_1, children = void 0, start = void 0, end = void 0, i = void 0, div = void 0;
                for (var key in arr) {
                    d = arr[key]["date"].split("-");
                    d = parseInt(d[2]) + "";
                    parent_1 = document.getElementById(d);
                    children = parent_1.getElementsByTagName("button");
                    start = arr[key]["start"].split(":");
                    start = parseInt(start[0]);
                    end = arr[key]["end"].split(":");
                    end = parseInt(end[0]);
                    for (i = start; i < end; i++) {
                        if (start < parseInt(children[i].value) && parseInt(children[i].value) < end) {
                            parent_1.removeChild(children[i]);
                            i -= 1;
                        }
                        else if (children[i].value == "yes" && parseInt(children[i].name) < end) {
                            children[start].value = "check";
                            children[i].style.marginTop = ((start - parseInt(children[i].name) + 1) * 40 + 10) + "";
                            children[i].className = "hour-btn attachment-btn first-half";
                        }
                    }
                    children[start].style.background = arr[key]["color"];
                    children[start].style.height = ((end - start) * 40 - 10) + "px";
                    div = document.createElement('div');
                    div.innerHTML = arr[key]["name"];
                    children[start].insertBefore(div, children[start].firstChild);
                    children[start].className = "hour-btn attachment-btn";
                    if (children[start].value == "check") {
                        children[start].value = "yes";
                        children[start].className = "opacity hour-btn attachment-btn ";
                    }
                    children[start].value = "yes";
                    children[start].name = "" + start;
                }
            }
        });
    };
    WeekComponent.prototype.hours = function (day) {
        var td = document.createElement('td');
        td.innerHTML = day;
        var button;
        for (var i = 0; i < 24; i++) {
            button = document.createElement('button');
            button.className = "hour-btn";
            button.setAttribute(this.attr, this.attr);
            button.setAttribute("data-target", "#myModal");
            button.setAttribute("data-toggle", "modal");
            if (i < 10)
                button.innerHTML = "0" + i + ":30";
            else
                button.innerHTML = i + ":30";
            button.value = "" + i;
            td.id = day;
            td.appendChild(button);
        }
        return td;
    };
    WeekComponent.prototype.Appointment = function () {
        var _this = this;
        var date = this.year + "-" + this.month + "-" + this.target.parentElement.id;
        var name = document.getElementById('appoint');
        if (name.value != "") {
            var from = document.getElementById('from');
            var to_1 = document.getElementById('to');
            var lat = document.getElementById('lat');
            var lng = document.getElementById('lng');
            this.appoint.postAppointment(name.value, from.value, to_1.value, date, this.colour, lat.value, lng.value).subscribe(function (result) {
                if (result === true) {
                    var i = 1;
                    var val = parseInt(to_1.value);
                    var remove = void 0;
                    while (i) {
                        remove = _this.target.nextSibling;
                        if (val > parseInt(remove.value)) {
                            i += 1;
                            _this.target.parentElement.removeChild(remove);
                        }
                        else
                            break;
                    }
                    ;
                    var div = document.createElement('div');
                    div.innerHTML = name.value;
                    _this.target.insertBefore(div, _this.target.firstChild);
                    _this.target.style.background = _this.colour;
                    _this.target.style.height = (i * 40 - 10) + "px";
                    _this.target.className = "hour-btn attachment-btn";
                }
                else {
                    console.error("Something went wrong");
                }
            });
        }
    };
    WeekComponent = __decorate([
        core_1.Component({
            selector: 'year-calendar',
            templateUrl: 'view/calendar/week.component.html',
            styleUrls: ['css/week.component.css',
                'css/month.component.css'],
            animations: [animations_1.slideAside]
        }), 
        __metadata('design:paramtypes', [])
    ], WeekComponent);
    return WeekComponent;
}(month_component_1.MonthComponent));
exports.WeekComponent = WeekComponent;
//# sourceMappingURL=week.component.js.map
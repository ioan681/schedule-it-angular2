import { Component, OnInit } 	from '@angular/core';
import { AuthService} 				from './login/auth.service';
import { User }              	from './models/user';
import './rxjs-operators';
@Component({
  templateUrl: 'view/navigation.component.html',
  styleUrls: ['css/week.component.css','css/login.component.css','css/month.component.css','css/navigation.component.css'],
  outputs: ['month'],
})
export class NavigationComponent implements OnInit{ 
	public isChecked = false;
	public isMapChecked = true;
	public isChanged = false;
	public day:any;;
	public month:any;;
	public image:any;;
	constructor(private auth: AuthService) {}
	ngOnInit(){
		let d = new Date();
		this.day = d.getMonth()+1;
		let user: User;
		var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    	let token = currentUser && currentUser.token;
		this.auth.getUser().subscribe(result => {
			user = result.json();
			localStorage.removeItem('currentUser');
			localStorage.setItem('currentUser', JSON.stringify(
				{name: user.name,email: user.email,telephone: user.telephone,
					profession: user.profession,about: user.about,img_name: user.img_name,
						token: token }));
			document.getElementById("profile-name").innerHTML=user.name;
			document.getElementById("side-name").innerHTML=user.name;
			document.getElementById("side-mail").innerHTML= "Mail: "+user.email;
			this.image = user.img_name;
		});
	}
	Click(){
		this.isChecked = !this.isChecked;
	}
	da(){
		let calendar = document.getElementById('calendar-content');
		this.isMapChecked = !this.isMapChecked;
		calendar.style.marginRight =(this.isMapChecked? '':'45%')
		let wrapper = document.getElementById('wrapper');
		let map:string, clas:string;
		clas =(this.isChecked? '':'toggled')
		map = (this.isMapChecked? '':' mapToggled')
		wrapper.className = clas + map;
		wrapper.setAttribute('ng-reflect-ng-class',wrapper.className)
	}
	Change(){
		this.isChanged = !this.isChanged;
	}
}

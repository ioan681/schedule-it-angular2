import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService} from './auth.service'

 
@Component({
    moduleId: module.id,
    templateUrl: '../../view/login.component.html',
    styleUrls: ['../../css/login.component.css']
})
 
export class LoginComponent{
    model: any = {};
    loading = false;
    error = '';

    constructor(private router: Router, private auth: AuthService) {}
    getUser() {
        this.auth.getUser().subscribe(result => {
            console.log(result)
        });
    }
    ngOnInit() {
        //this.getUser();
        // reset login status
        this.auth.logout();
        // document.getElementById('icons').appendChild(document.getElementById('my-signin3'))
        // document.getElementById('my-signin3').style.display="inline";
    }

    login() {
        this.loading = true;
        this.auth.login(this.model.email, this.model.pass)
            .subscribe(result => {
                if (result === true) {
                    this.router.navigate(['/schedule']);
                } else {
                    this.error = 'Username or password is incorrect';
                    this.loading = false;
                }
            },
            error => {
                    this.error = 'Username or password is incorrect';
                    this.loading = false;
                });
    }
}
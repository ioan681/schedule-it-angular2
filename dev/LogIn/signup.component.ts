import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService} from './auth.service'

 
@Component({
    moduleId: module.id,
    templateUrl: '../../view/signup.component.html',
    styleUrls: ['../../css/login.component.css']
})
 
export class SignUpComponent{
    model: any = {};
    loading = false;
    public passCheck:boolean = true;
    error = '';

    constructor(private router: Router, private auth: AuthService) {}
    onKey(value: string) {
        this.passCheck =( this.model.pass == value);
    }
    getUser() {
        this.auth.getUser().subscribe(result => {
            console.log(result)
        });
    }
    ngOnInit() {
        // reset login status
        this.auth.logout();
        // document.getElementById('login').appendChild(document.getElementById('my-signin3'))
        // document.getElementById('my-signin3').style.display="inline";
    }

    signUp() {
        this.loading = true;
        this.auth.signUp(this.model.username,this.model.email, this.model.pass)
            .subscribe(result => {
                if (result === true) {
                    this.router.navigate(['/']);
                } else {
                    this.error = 'Username or password is incorrect';
                    this.loading = false;
                }
            },
            error => {
                    this.error = 'Username or password is incorrect';
                    this.loading = false;
                });
    }

}
import { Injectable } from '@angular/core';
import { Router, CanActivate, CanDeactivate } from '@angular/router';
 
@Injectable()
export class AuthGuard implements CanActivate {
 
    constructor(private router: Router) { }
 
    canActivate() {
        if (localStorage.getItem('currentUser')) {
            // logged in so return true
            return true;
        }
 
        // not logged in so redirect to index page
        this.router.navigate(['/']);
        return false;
    }
    canDeactivate() {
        if (localStorage.getItem('currentUser')) {
            // logged in so return true
            return false;
        }
     
        return true;
    }
}
import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';
import { AuthGuard } from './login/auth.guard';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './login/signup.component';
import { YearComponent }   from './calendar/year.component';
import { LandingComponent} from './landing/landing.component';


const appRoutes: Routes = [
	{ path: 'login', component: LoginComponent },
	{ path: 'signup', component: SignUpComponent },
  //{ path: '', component: LandingComponent },
  { path: '**', redirectTo: '/schedule' }
  
];

export const routing = RouterModule.forRoot(appRoutes);
@NgModule({
  imports: [
	RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers: [
  ]
})
export class AppRoutingModule {}
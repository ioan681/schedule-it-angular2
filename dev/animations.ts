import { animate, AnimationEntryMetadata, state, style, 
    transition, trigger } from '@angular/core';
// Component transition animations
export const slideAside: AnimationEntryMetadata =
  trigger('flyInOut', [
      // state('in', style({transform: 'translateX(0)'})),
	      state('prev', style({
	        transform: 'translateX(-2000px)',
	      })),
	      state('next', style({
	        transform: 'translateX(2000px)',
	      })),
        transition('next => right', [
		      style({transform: 'translateX(-500px)'}),
		      animate(250)
          ]),
        transition('prev => left', [
          style({transform: 'translateX(500px)'}),
          animate(250)
        ]),
	        transition('* => prev', animate('350ms ease-in')),
	        transition('* => next', animate('350ms ease-in')),
    ])
	    
  
import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';
import { User }              from '../models/user';
import { Observable }        from 'rxjs/Observable';
import { Subject }           from 'rxjs/Subject';
import { UserService} from './user.service';
@Component({
  selector: 'user-search',
  templateUrl: 'view/user/user-search.component.html',
  //styleUrls: [ 'hero-search.component.css' ],
})
export class UserSearchComponent implements OnInit {
  public result:any;
  public users: User[];
  constructor(
    private UserService: UserService,
    private router: Router) {}
  ngOnInit(){
    
  }
  Click(){
    if(this.result == undefined)
      this.UserService.getUsers().subscribe(
                  result => {this.result = JSON.parse(result["_body"])});
  }
  search(value:any){
    if(value!=""){
      if(this.result != null){
        let arr:any = [];
        this.result.forEach(function(element:any) {
            if(element["name"].search(value)>=0){
              arr.push(element);
            }
        });
        this.users = arr;
      }
    }
    else this.users = null;
  }
  gotoUser(user: User): void {
    this.users = null;
    localStorage.setItem('selectedUser', JSON.stringify(
				{id: user.id, name: user.name,email: user.email,telephone: user.telephone,
					profession: user.profession,about: user.about,img_name: user.img_name}));
    let link = ['/user', user.id];
    this.router.navigate(link);
  }
}

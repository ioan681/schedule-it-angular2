import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import { User }           from '../models/user';
import { Group }           from '../models/group';
@Injectable()
export class GroupService {
    public token:string;
    constructor(private http: Http) {
        // set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }
		createGroup(id:number,name: string, description: string){
			let headers = new Headers({ 'Authorization': 'Bearer '+ this.token})
			headers.append('Content-Type','application/json,multipart/form-data');
			let options = new RequestOptions({ headers: headers });
			return this.http.post('http://schedule.mvm.bg/createGroup', JSON.stringify(
					{id:id, name:name, description: description}),options)
									.map((response: Response) => {
											return response;
									})
		}
		getUserGroups(id:number){
			let headers = new Headers({ 'Authorization': 'Bearer '+ this.token})
			headers.append('Content-Type','application/json');
			let options = new RequestOptions({ headers: headers });
			return this.http.get('http://schedule.mvm.bg/groups/'+id,options)
									.map((response: Response) => {
											return response;
									})
		}
		addtoGroup(user_id:number,id:number){
			let headers = new Headers({ 'Authorization': 'Bearer '+ this.token})
			headers.append('Content-Type','application/json');
			let options = new RequestOptions({ headers: headers });
			return this.http.post('http://schedule.mvm.bg/addToGroup', JSON.stringify(
					{user_id:user_id,id:id}),options)
									.map((response: Response) => {
											return response;
									})
		}
}
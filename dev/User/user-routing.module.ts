import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../login/auth.guard';
import { ProfileComponent } from './profile.component';
import { UserComponent } from './user.component';
import { NavigationComponent } from '../navigation.component';
const CalendarRoutes: Routes = [
  
  { path: 'schedule',
    component: NavigationComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'user/:id', component: UserComponent},
      { path: 'profile', component: ProfileComponent},
    ] 
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(CalendarRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class UserRoutingModule { }
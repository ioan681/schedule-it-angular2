import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import { User }           from '../models/user';
@Injectable()
export class UserService {
    public token:string;
    constructor(private http: Http) {
        // set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }
    private handleError (error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
          const body = error.json() || '';
          const err = body.error || JSON.stringify(body);
          errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
          errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
    update(user: Object){
        let headers = new Headers({ 'Authorization': 'Bearer '+ this.token})
        headers.append('Content-Type','application/json');
        let options = new RequestOptions({ headers: headers });
        return this.http.post('http://schedule.mvm.bg/update', JSON.stringify(
            {name: user["username"],email: user["email"],telephone: user["telephone"],
				profession: user["profession"],about: user["about"]}),options)
                    .map((response: Response) => {
                        return response;
                    })
    }
    getUser(id:number){
        let headers = new Headers({ 'Authorization': 'Bearer '+ this.token})
        headers.append('Content-Type','application/json');
        let options = new RequestOptions({ headers: headers });
        let Url = 'http://schedule.mvm.bg/user/'+id;
        return this.http.get(Url,options).catch(this.handleError);             
    }
    getUsers(){
        let headers = new Headers({ 'Authorization': 'Bearer '+ this.token})
        headers.append('Content-Type','application/json');
        let options = new RequestOptions({ headers: headers });
        return this.http.get('http://schedule.mvm.bg/users',options).map((response: Response) => {
                        return response;
                    }).catch(this.handleError);
    }
    getImage(img:string){
        let headers = new Headers({ 'Authorization': 'Bearer '+ this.token})
        headers.append('Content-Type','multipart/form-data');
        let options = new RequestOptions({ headers: headers });
        return this.http.get('http://schedule.mvm.bg/profile/'+img,options).map((response: Response) => {
                        return response;
                    });
    }
}
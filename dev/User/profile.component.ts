import { Component, OnInit } from '@angular/core';
import { UserService} from './user.service'
import { FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
import '../rxjs-operators';
const URL = 'http://schedule.mvm.bg/profile2';
if(JSON.parse(localStorage.getItem('currentUser')) == null){
  localStorage.setItem('currentUser', JSON.stringify(
				{token: "da"}));
}
const Profile = "Bearer "+ JSON.parse(localStorage.getItem('currentUser')).token;
@Component({
  templateUrl: 'view/user/profile.component.html',
  styleUrls: ['css/user.component.css'],
  
})
export class ProfileComponent implements OnInit{ 
  public upload:Boolean = false;
  public save:any;
  public passCheck:boolean = true;
  private token:string;
  private image:any;
  model: any = {
  };
  constructor(private service:UserService){}
	ngOnInit(){
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.model.username = currentUser.name;
    this.model.email = currentUser.email;
    this.model.telephone = currentUser.telephone;
    this.model.profession = currentUser.profession;
    this.model.about = currentUser.about;
    this.token = currentUser.token;
    this.image = currentUser.img_name;
  }
  onKey(value: string) {
        this.passCheck =( this.model.pass == value);
  }
  public uploader:FileUploader = new FileUploader({url: URL, authToken: Profile});
  Image(event:any){
    if(event.srcElement.files[0]) {
      this.model.file = event.srcElement.files[0];
      this.upload = true;
      document.getElementById("picture-class").className="btn btn-success btn-file";
      this.save = <HTMLImageElement>document.getElementById('user_img');
      this.save = this.save.src;
      var reader = new FileReader();
      reader.onload = function(){
        var output = <HTMLImageElement>document.getElementById('user_img');
        var output2 = <HTMLImageElement>document.getElementById('profile-pic');
        output.src = reader.result;
        output2.src = reader.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }
    else{ 
      this.upload = false;
      document.getElementById("picture-class").className="btn btn-primary btn-file"
    }
  }
  Update(){
    this.service.update(this.model)
          .subscribe(result => {
              localStorage.removeItem('currentUser');
			        localStorage.setItem('currentUser', JSON.stringify(
				{name: this.model.username,email: this.model.email,telephone: this.model.telephone,
					profession: this.model.profession,about: this.model.about , token: this.token }));
            },
            );
  }
  Close(event:any){
    if(event.srcElement.value != "1"){
      setTimeout(() =>{
        if(this.upload == true && document.getElementById("myModal").className == "modal fade"){
        let test = <HTMLImageElement>document.getElementById('user_img');
        test.src = this.save;
        test = <HTMLImageElement>document.getElementById('profile-pic');
        test.src = this.save;
      }
      }, 100);
    }
  }
}

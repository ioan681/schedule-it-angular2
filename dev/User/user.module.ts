import { NgModule }         from '@angular/core';
import { CommonModule }     from '@angular/common';
import { FormsModule }      from '@angular/forms';
import { UserRoutingModule} from './user-routing.module';
import { FileUploadModule } from 'ng2-file-upload';

import { ProfileComponent } from './profile.component';
import { UserComponent }    from './user.component';

import { GroupService} 			from './group.service'
import { UserService}       from './user.service';
@NgModule({
  imports: [
    CommonModule,
    FileUploadModule,
    FormsModule,
    UserRoutingModule,
    
  ],
  declarations: [
    ProfileComponent,
    UserComponent
  ],
  providers: [
    UserService,
    GroupService
  ]
})
export class UserModule {}
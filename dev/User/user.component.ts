import { Component, OnInit,ElementRef, Renderer} from '@angular/core';
import { UserService} 				from './user.service'
import { GroupService} 				from './group.service'
import { User }              	from '../models/user';
import { Group }              from '../models/group';
import { Router, ActivatedRoute, Params } from '@angular/router';
import '../rxjs-operators';
@Component({
  templateUrl: 'view/user/user.component.html',
  styleUrls: ['css/user.component.css'],
})
export class UserComponent implements OnInit{ 
  private model: User;
	private group = new Group;
	globalListenFunc: Function;
	private groups: Group[];
	private userGroups: Group[];
  constructor(
		private service:UserService,
		private groupService: GroupService,
		private router: Router,
		private route: ActivatedRoute,
		elementRef: ElementRef, 
		renderer: Renderer){
		this.globalListenFunc = renderer.listenGlobal('document', 'click', (event:any) => {
        let id:any;
				this.route.params.forEach((params: Params) => {
					id = +params['id'];
					});
					if(id != this.model.id)
				this.service.getUser(id).subscribe(
						result => {this.model = JSON.parse(result["_body"]);
				});
    });
		
	}
	ngOnInit(){
		this.model = JSON.parse(localStorage.getItem('selectedUser'));	
  }
	newGroup(){
		let group = document.getElementById('new-group');
		let btn = document.getElementById('new-btn');
		if(group.className == "new-group closed") {
			group.className = "new-group";
			btn.className = "btn-primary btn";
		}
		else{
			 group.className = "new-group closed";
			 btn.className = "btn-success btn";
		}
	}
	Create(){
		this.groupService.createGroup(this.model.id,this.group.name,this.group.description).subscribe();
	}
	Groups(){
		this.groupService.getUserGroups(this.model.id).subscribe(
									result => {
										let arr = JSON.parse(result["_body"]);
										this.groups = arr[0];
										this.userGroups = arr[1];
									});
	}	
	Check(group:any){
		let check = false;
		if(this.userGroups != undefined)
		this.userGroups.forEach(element => {
			if(group['id'] == element['id']) {
				check = true;
			}
		});
		return check;
	}
	Add(id:number){
		this.groupService.addtoGroup(this.model.id,id).subscribe(
									result => {
										this.userGroups.push( JSON.parse(result["_body"]) )
									});
	}
}

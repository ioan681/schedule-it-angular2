import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule }        from './app-routing.module';
import { HttpModule } from '@angular/http';  


import { CalendarModule } from './calendar/calendar.module';
import { UserModule } from './user/user.module';


import { NavigationComponent } from './navigation.component';
import { AppComponent }   from './app.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './login/signup.component';
import { UserSearchComponent } from './user/user-search.component';

import { AuthGuard } from './login/auth.guard';
import { AuthService} from './login/auth.service';

import { LandingComponent} from  './landing/landing.component';


@NgModule({
  imports:      [ 
    FormsModule,
  	BrowserModule,
    HttpModule,
    CalendarModule,
    UserModule,
  	AppRoutingModule
  ],
  declarations: [ 
    AppComponent,
    LoginComponent,
    SignUpComponent,
    NavigationComponent,
    UserSearchComponent,
    LandingComponent
    
    ],
  providers:  [
     AuthGuard,
     AuthService,
  ],
  bootstrap:    [ AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User }   from '../models/user';
 
@Component({
    moduleId: module.id,
    templateUrl: '../../view/landing.component.html',
    //styleUrls: ['../../css/login.component.css']
})
 
export class LandingComponent{
    private user: User;
    ngOnInit(){
		this.user = JSON.parse(localStorage.getItem('currentUser'));	
  }
}
import {Component,Input,trigger,state,style,transition,OnInit,
  animate} from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import {CalendarService} from './calendar.service';
import {slideAside } from '../animations';
@Component({
  selector: 'year-calendar',
  templateUrl: 'view/calendar/month.component.html',
  styleUrls: ['css/month.component.css'],
  animations: [slideAside]
})
export class MonthComponent implements OnInit { 
	public lastday:number;
	public day:any;
	public month:number;
	public month2:string = "January"; 
	public week1:number[];
	public week2:number[]; 
	public week3:number[];
	public week4:number[];
	public week5:number[];
	public week6:number[];
	public week7:number[];
	public state = "in";
	public year:number;
	public cont2:any;
	public attr:any;
	public sideContainer:any;
	public sideMonth:any;
	constructor(private route: ActivatedRoute,public appoint: CalendarService) {}
	ngOnInit(){
		this.GetCurrentYear();
		this.route.params.forEach((params: Params) => {
				this.month = +params['id'];
				this.day = this.month;
			});
		this.cont2 = document.getElementById("MonthContainer2");
		this.attr = this.cont2.attributes[0]["nodeName"]; 
		this.sideContainer = document.getElementById("side-img");
		this.sideMonth = document.getElementById("month");
		var side = document.getElementById('side-month-input')
		side.style.display = "block";
		document.getElementById('side-month').appendChild(side);
		this.month -=1;
		this.MonthCreate(1);
		
	}
	GetCurrentYear(){
		let date = new Date();
		this.month = date.getMonth()+1;
		this.year = date.getFullYear();
		//not sure
		this.lastday = date.getDate();
	}
	MonthDays(){
		this.lastday = 0;
			let mon:number;
		if(this.month <= 7){
			if(this.month % 2 == 1) mon = 30;
			else mon = 31;
			if(this.month == 1) mon = 31;
			if(this.month == 3){
				if(((this.year % 4 == 0) && (this.year % 100 != 0)) 
					|| (this.year % 400 == 0))mon = 29;
				else mon = 28;
			} 
		}
		else{
			if(this.month % 2 == 1) mon = 31;
			else mon = 30;
		}
		let j=1;
		let d = new Date(this.year,(this.month-1),1);
		let day = d.getDay();
		let today = d.getDate();
		let dif = today % 7;
		let days = new Array;
		if( day - dif <0){
			dif = day - dif + 8;
		}
		else dif = day-dif+1;
		for (let i = 0; i != 7; i+=1) {
			if(i < dif-1) days.push(mon-dif+i+2);
			else {
				days.push(j);
				j+=1;
			}
		}
		this.lastday = j-1;
		return days;
	}
	MonthWeek(){
		let mon:number;
		if(this.month <= 7){
			if(this.month % 2 == 1) mon = 31;
			else mon = 30;
			if(this.month == 2){
				if(((this.year % 4 == 0) && (this.year % 100 != 0)) 
					|| (this.year % 400 == 0))mon = 29;
				else mon = 28;
			}
		}
		else{
			if(this.month % 2 == 1) mon = 30;
			else mon = 31;
		}
		let j = this.lastday;
		let days = new Array;
		for (var i = 0; i < 7;i+=1) {
			if(j == mon) j = 0;
			j +=1;
			days.push(j)
		}
		this.lastday = j;
		return days;
	}
	MonthCreate(n:any){
		var container = document.getElementById("MonthContainer");
		var div = container;
		div.innerHTML = "";
		var table = document.createElement('table');
		var tbody = document.createElement('tbody');
		table.className = "table table-bordered";
		switch (n) {
				case -1:{
					if(this.month == 1){
						this.month = 12;
						this.year -=1;
					}
					else this.month-=1;
					break;
				}
				case 0:{
					this.GetCurrentYear();
					break;
				}
				case 1:{
					if(this.month == 12){
						this.month = 1;
						this.year +=1;
					}
					else this.month+=1;
					break;
			}
		}
		var tr = this.Content(this.MonthDays(),"day",1);
		tbody.appendChild(tr);
		for (var i=0; i < 4;i ++) {
			tr = this.Content(this.MonthWeek(),"day",2);
			tbody.appendChild(tr);
		}
		tr = this.Content(this.MonthWeek(),"day",3);
		tbody.appendChild(tr);
		table.appendChild(tbody);
		table.setAttribute(this.attr, this.attr);
		this.cont2.removeChild(container);
		div.appendChild(table);
		this.cont2.appendChild(div);
		this.Month();
		return table;
	}
	Content(week:any,clas:any,type:any){
		let td:any, check:number;
		let tr = document.createElement('tr');
		tr.setAttribute(this.attr, this.attr);
		if(week[0]>week[6] && type==1) check=1;
		if(week[0]>week[6] && type==2) check=2;
		if(week[0]<week[6] && type==3) check=3;
		week.forEach((element:any) => {
			td = document.createElement('td');
			td.innerHTML = element;
			td.setAttribute(this.attr, this.attr);
			td.className = clas;
			if(check == 1 && element>7){
				td.className = clas +" inactive";
			}
			if(check == 2 && element<7){
				td.className = clas +" inactive";
			}
			if(check == 3){
				td.className = clas +" inactive";
			}
			tr.appendChild(td);
		});
		return tr;
	}
	Prev(){
		this.state = "prev";
		this.MonthCreate(-1);
		setTimeout(() =>{
		  this.state="left";
		}, 4000);
	}
	Next(){
		this.state = "next";
		this.MonthCreate(1);
		setTimeout(() =>{
		  this.state="right";
		}, 4000);
	}
	Month(){
		switch (this.month) {
					case 1: this.month2 = "January";break;
					case 2: this.month2 = "February";break;
					case 3: this.month2 = "March";break;
					case 4: this.month2 = "April";break;
					case 5: this.month2 = "May";break;
					case 6: this.month2 = "June";break;
					case 7: this.month2 = "July";break;
					case 8: this.month2 = "August";break;
					case 9: this.month2 = "September";break;
					case 10:this.month2 = "October";break;
					case 11:this.month2 = "November";break;
					case 12:this.month2 = "December";break;
			}
			this.SideTable();
	}
	SideTable(){
		// side img
		this.sideContainer.innerHTML = "";
		var img = document.createElement("img");
		img.className = "img-circle year-image";
		img.src = "src/"+this.month2+".jpg";
		img.setAttribute(this.attr, this.attr);
		this.sideContainer.appendChild(img);

		// side month
		var table = document.createElement('table');
		var tbody = document.createElement('tbody');
		table.className = "side-table";
		let save{
			month : this.month,
			day : this.lastday,
			year : this.year
		}
		var tr = this.Content(this.MonthDays(),"",1);
		tbody.appendChild(tr);
		for (var i=0; i < 4;i ++) {
			tr = this.Content(this.MonthWeek(),"",2);
			tbody.appendChild(tr);
		}
		tr = this.Content(this.MonthWeek(),"",3);
		this.month = save.month;
		this.lastday = save.day;
		this.year = save.yea;
		tbody.appendChild(tr);
		table.appendChild(tbody);
		table.setAttribute(this.attr, this.attr);
		this.sideMonth.innerHTML = "";
		this.sideMonth.appendChild(table);
	}
  
}

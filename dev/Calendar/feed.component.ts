import { Component } from '@angular/core';
import { WeekComponent }   from './week.component';
import { MonthComponent} from './month.component';

import {CalendarService} from './calendar.service';

@Component({
  selector: 'year-calendar',
  templateUrl: 'view/calendar/feed.component.html',
  styleUrls: ['css/feed.component.css']
})
export class FeedComponent extends WeekComponent{ 
	
	private result:any;
	protected textClass:any;
	private monthComponent:MonthComponent;
	protected tmpTask:any; //For the modal window. This temporary variable is 
						 // filled when you click on a event of the feed.

	ngOnInit(){
		
		this.sideContainer = document.getElementById("side-img");
		this.attr = this.sideContainer.attributes[0]["nodeName"];
    	this.sideMonth = document.getElementById("month");
		super.GetCurrentYear();
		super.Month();
		super.SideTable();
		
		let date:any = this.getToday();
		this.appoint.getAppointment(date).subscribe(result=>{this.result =result; console.log(result)});
	}

	getToday(){
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		return yyyy+'-'+mm+'-'+dd;
	}
	hexToRgb(hex:any,opacity:any) {
	    var h=hex.replace('#', '');
        h =  h.match(new RegExp('(.{'+h.length/3+'})', 'g'));

        for(var i=0; i<h.length; i++)
            h[i] = parseInt(h[i].length==1? h[i]+h[i]:h[i], 16);

        if (typeof opacity != 'undefined')  h.push(opacity);

        return 'rgba('+h.join(',')+')';
	}
	getDayFromDate(date:any){
		return date.split('-')[2];
	}
	getStartEndFromDate(start:any, end:any){
		start=start.split(':');
		end=end.split(':');
		return start[0]+":"+start[1]+" - "+end[0]+":"+end[1]; 
	}
	getColor(color:any){
		let colorsJSON={
			myClass:'',
			background:'',
			taskClass:''
		};
		switch(color){
			case "blue":
			colorsJSON.myClass= "text-info"; 
			colorsJSON.taskClass= "task-info";
			break;
			case "red": 
			colorsJSON.myClass="text-danger"; 
			colorsJSON.taskClass= "task-danger";
			break;
			case "purple": 
			colorsJSON.myClass= "text-primary";
			colorsJSON.taskClass= "task-primary"; 
			break;
			case "yellow": 
			colorsJSON.myClass= "text-warning"; 
			colorsJSON.taskClass= "task-warning";
			break;
			case "green": 
			colorsJSON.myClass="text-success"; 
			colorsJSON.taskClass= "task-success";
			break;
			default:
			colorsJSON.myClass= "text-muted"; 
			colorsJSON.taskClass= "task-muted";
			break;
		}
		return colorsJSON;
	}


}

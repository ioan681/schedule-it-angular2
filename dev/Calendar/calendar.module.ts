import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AgmCoreModule } from 'angular2-google-maps/core';

import { MapComponent,AddMarkerMapComponent } from '../map.component';
import { YearComponent }   from './year.component';
import { MonthComponent }   from './month.component';
import { WeekComponent }   from './week.component';
import { FeedComponent} from './feed.component';

import { FeedService} from './feed.service';
import { CalendarService} from './calendar.service';
import { CalendarRoutingModule} from './calendar-routing.module';
@NgModule({
  imports: [
    CommonModule,
    CalendarRoutingModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBleBTzUcdI09CD7svMvqIjiST5k8yUTyo'
    }),
  ],
  declarations: [
	  YearComponent,
    MonthComponent,
    WeekComponent,
    FeedComponent,
    MapComponent,
    AddMarkerMapComponent,
  ],
  providers: [
    CalendarService,
    FeedService
  ]
})
export class CalendarModule {}
import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../login/auth.guard';
import { YearComponent }   from './year.component';
import { MonthComponent }   from './month.component';
import { WeekComponent }   from './week.component';
import { NavigationComponent } from '../navigation.component';
import { FeedComponent} from './feed.component';
const CalendarRoutes: Routes = [
  
  { path: 'schedule',
    component: NavigationComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'year', component: YearComponent },
      { path: 'month/:id', component: MonthComponent },
      { path: 'week', component: WeekComponent },
      { path: '', component: FeedComponent },
      { path: 'feed', component: FeedComponent}
    ] 
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(CalendarRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class CalendarRoutingModule { }
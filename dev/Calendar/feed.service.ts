import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
@Injectable()
export class FeedService {
    public token: string;
    constructor(private http: Http) {
        // set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser.token;
    }    
    getMyAppointments(date: string): Observable<any>{
        let headers = new Headers({ 'Authorization': 'Bearer '+ this.token });
        headers.append('Content-Type','application/json');
        let options = new RequestOptions({ headers: headers });
        return this.http.post('http://schedule.mvm.bg/getAppointments',
         JSON.stringify({date: date }),options)
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let token = response.json();
                if (token) return token["attach"];
                else return false;
                
            }).catch(this.handleError);
    }
    private handleError (error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
          const body = error.json() || '';
          const err = body.error || JSON.stringify(body);
          errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
          errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}
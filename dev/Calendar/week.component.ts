import {Component,Input,trigger,state,style,transition,OnInit,
	animate, Directive} from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import {slideAside } from '../animations';
import {CalendarService} from './calendar.service';
import { MonthComponent} from './month.component';
import { AddMarkerMapComponent} from '../map.component';
declare var google:any;
declare var $:any;
@Component({
  selector: 'year-calendar',
  templateUrl: 'view/calendar/week.component.html',
  styleUrls: ['css/week.component.css',
  'css/month.component.css'],
  animations: [slideAside]
})
export class WeekComponent extends MonthComponent {
  public target: any;
  public colourVal:number=1;
  public colour:string = "#0275d8";
	public week:number[];
	public year2:number;
	public left:number[];
	public right:number[];

	ngOnInit(){
    
		//console.log(document.getElementById("side-container"))
		this.cont2 = document.getElementById("weekContainer2");
		this.attr = this.cont2.attributes[0]["nodeName"];
		this.sideContainer = document.getElementById("side-img");
    this.sideMonth = document.getElementById("month");
		// var side = document.getElementById('side-month-input')
		// side.style.display = "block";
		//document.getElementById('side-month').appendChild(side);
    this.Create(0);
	}
	Days(){
		// this.route.params.forEach((params: Params) => {
		// 	this.month = +params['id'];
		// 	});
		let mon:number;
		this.year = (new Date()).getFullYear()
		let d = new Date();
		let day = d.getDay();
		let today = d.getDate();
		this.month = d.getMonth()+1;
		console.log(today)
		if(today-day<0) {
			if(this.month == 1){
				this.month = 12;
				this.year -= 1;
			}
			else{
				this.month -=1;
			}
			let obj = {mon: 0}
			this.GetMonth(obj);
			this.lastday = obj.mon + today - day;
		}
		else this.lastday = today-day;
		return this.Week();
	}
	Week(){
		var obj = {mon: 0}
		this.GetMonth(obj);
		let j = this.lastday;
		let days = new Array;
		if(this.lastday-7 < 0){
			if(this.month == 1){this.month = 12; this.year -=1;}
			else this.month -=1;
		}
		for (var i = 0; i < 7;i+=1) {
			if(j == obj.mon) j = 0;
			j +=1;
			days.push(j)
		}
		this.lastday = j;
		if(days[0] < days[6] && days[0]-7<=0){
			if(this.month == 12){this.month = 1; this.year +=1;}
			else this.month +=1;
		}
		this.Month();
		if(days[0] > days[6] && days[6]-7<=0){
			if(this.month == 12){this.month = 1; this.year +=1;}
			else this.month +=1;
		}
		return days;
	}
	Week1(){
		var obj = {mon: 0}
		if(this.lastday - 14 >= 0){
			this.lastday -=14;
			this.GetMonth(obj);
		}
		else {
			if(this.month == 1){
				this.month = 12;
				this.year -= 1;
				this.GetMonth(obj);
			}
			else{
				this.month -=1;
				this.GetMonth(obj);
			}
			this.lastday = (this.lastday - 14 + obj.mon);
		}
		let days = new Array;
		let j = this.lastday;
		for (var i = 0; i < 7;i+=1) {
			if(j == obj.mon) j = 0;
			j +=1;
			days.push(j)
		}
		this.lastday = j;
		if(this.lastday - 7 < 0){
			if(this.month == 12){
				this.month = 1;
				this.year +=1;
			}
			else this.month +=1;
		}
		this.Month();
		return days;
	}
	GetMonth(obj: any){
		if(this.month <= 7){
			if(this.month % 2 == 1) obj.mon = 31;
			else obj.mon = 30;
			if(this.month == 2){
				if(((this.year % 4 == 0) && (this.year % 100 != 0)) 
					|| (this.year % 400 == 0))obj.mon = 29;
				else obj.mon = 28;
			}
		}
		else{
			if(this.month % 2 == 1) obj.mon = 30;
			else obj.mon = 31;
		}
	}
	Create(n: any){
		var container = document.getElementById("weekContainer");
		var div = container;
		div.innerHTML = "";
		var table = document.createElement('table');
		var tbody = document.createElement('tbody');
		tbody.className = "year table table-bordered";
		var tr = document.createElement('tr');
		//tr.setAttribute(this.attr, this.attr);
		table.className = "year table table-bordered";
		var td:any, week:any;
		switch (n) {
			case -1:week = this.Week1();break;
			case 0:week = this.Days();break;
			case 1:week = this.Week();break;
		}
		week.forEach((element:any) => {
      td = this.hours(element);
      tr.appendChild(td);
	  });
			this.showAttach(week[0]);
      tbody.appendChild(tr);
      table.appendChild(tbody);
      table.setAttribute(this.attr, this.attr);
      this.cont2.removeChild(container);
      div.appendChild(table);
      this.cont2.appendChild(div);
	}
	Prev(){
		this.state = "prev";
		this.Create(-1);
		setTimeout(() =>{
			this.state="left";
		}, 450);
	}
	Next(){
		this.state = "next";
		this.Create(1);
		setTimeout(() =>{
			this.state="right";
		}, 450);
	}
	Month(){
		switch (this.month) {
      case 1: this.month2 = "January";break;
      case 2: this.month2 = "February";break;
      case 3: this.month2 = "March";break;
      case 4: this.month2 = "April";break;
      case 5: this.month2 = "May";break;
      case 6: this.month2 = "June";break;
      case 7: this.month2 = "July";break;
      case 8: this.month2 = "August";break;
      case 9: this.month2 = "September";break;
      case 10:this.month2 = "October";break;
      case 11:this.month2 = "November";break;
      case 12:this.month2 = "December";break;
    }
    this.year2 = this.year;
    super.SideTable();
	}
  Color(val: any){
		switch(val){
			case 1:this.colour = "blue";this.colourVal=1;break;
			case 2:this.colour = "green";this.colourVal=2;break;
			case 3:this.colour = "red";this.colourVal=3;break;
			case 4:this.colour = "purple";this.colourVal=4;break;
			case 5:this.colour = "yellow";this.colourVal=5;break;
		}
	}
  clicked(event: any){
    let modal = document.getElementById('myModal');
    let from = <HTMLInputElement>document.getElementById('from');
    let to = <HTMLInputElement>document.getElementById('to');
    let val = event.target.innerHTML;
    this.target = event.target;
    from.value = val;
    val = val.split(":");
    let val2 = val[1];
    val = (parseInt(val[0])+1);
    if(val<10) val = "0"+val;
    val = val +":"+val2;
    to.value = val;
    
	setTimeout(()=>{
		let map=document.getElementById('map');
		google.maps.event.trigger(map, "resize");

	},250);   
  }
	showAttach(day: any){
    let date = this.year+"-"+this.month+"-"+day;
    let arr = new Array;
    this.appoint.getAppointment(date).subscribe(
			result => {
				if(result.length>0){
					arr = result;
					let d:any,parent:any,children:any,start:any,end:any,i:any,div:any;
					for(let key in arr){
						d = arr[key]["date"].split("-");
						d = parseInt(d[2])+"";
						parent = document.getElementById(d);
						children = parent.getElementsByTagName("button");
						start = arr[key]["start"].split(":");
						start = parseInt(start[0])
						end = arr[key]["end"].split(":");
						end = parseInt(end[0]);
						for(i=start;i<end;i++){
							if(start < parseInt(children[i].value) && parseInt(children[i].value)<end){
								parent.removeChild(children[i]);
								i-=1;	
							}
							else if(children[i].value == "yes" && parseInt(children[i].name)<end ){
								children[start].value = "check";
								children[i].style.marginTop = ((start - parseInt(children[i].name)+1)*40+10)+"";
								children[i].className = "hour-btn attachment-btn first-half";
							}
						}
						children[start].style.background = arr[key]["color"];
						children[start].style.height = ((end-start)*40-10)+"px";
						div = document.createElement('div');
						div.innerHTML = arr[key]["name"]; 
						children[start].insertBefore(div, children[start].firstChild);
						children[start].className = "hour-btn attachment-btn";
						if(children[start].value == "check"){
							children[start].value = "yes";
							children[start].className = "opacity hour-btn attachment-btn ";
						}
						children[start].value = "yes";
						children[start].name = ""+start;
					}
				 }
			}); 
	}
  hours(day: any){
    let td = document.createElement('td');
    td.innerHTML = day;
    let button:any;
	
		for(let i=0;i<24;i++){
			button = document.createElement('button');
			button.className = "hour-btn";
			button.setAttribute(this.attr, this.attr);
			button.setAttribute("data-target","#myModal");
			button.setAttribute("data-toggle","modal");
			if(i<10) button.innerHTML = "0"+i+":30";
			else button.innerHTML = i+":30";
			button.value = ""+i;
			td.id = day;
			td.appendChild(button);
		}
    return td;
  }
  Appointment(){
    let date = this.year+"-"+this.month+"-"+this.target.parentElement.id;
    let name = <HTMLInputElement>document.getElementById('appoint');
    if(name.value !=""){
      let from = <HTMLInputElement>document.getElementById('from');
      let to = <HTMLInputElement>document.getElementById('to');
      let lat = <HTMLInputElement>document.getElementById('lat');
      let lng = <HTMLInputElement>document.getElementById('lng');
      this.appoint.postAppointment(name.value,from.value,to.value,date,this.colour,lat.value,lng.value).subscribe(
            result => {
              if (result === true) {
                let i=1;
                let val = parseInt(to.value);
                let remove:any;
                while(i){
                  remove = this.target.nextSibling;
                  if(val > parseInt(remove.value)){
                    i+=1;
                    this.target.parentElement.removeChild(remove);
                  }
                  else break;
                };
				let div = document.createElement('div');
				div.innerHTML = name.value; 
				this.target.insertBefore(div, this.target.firstChild);
				this.target.style.background = this.colour;
				this.target.style.height = (i*40-10)+"px";
				this.target.className = "hour-btn attachment-btn";
              } else {
                  console.error("Something went wrong");
              }
      });
    }
  }
}

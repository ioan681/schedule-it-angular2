import { Component } from '@angular/core';
import { FeedComponent }   from './Calendar/feed.component';


import {CalendarService} from './Calendar/calendar.service';

@Component({
  selector: 'map',
  templateUrl: 'view/map.component.html',
  //styleUrls: ['app.component.css'],
  providers:[CalendarService]
})
export class MapComponent {
  private result:any;
  map:any;
  title: string = 'My first angular2-google-maps project';
  lat: number = 42.605650;
  lng: number = 23.032772;

  constructor(private appoint: CalendarService){}
  ngOnInit(){
  	let date:any = this.getToday();
  	this.appoint.getAppointment(date).subscribe(result=>{
  		this.result =result; 
  		console.log(result);
  		this.initMap();
  	});
  }
  getToday(){
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		return yyyy+'-'+mm+'-'+dd;
	}
  initMap() {
        this.map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: this.lat, lng: this.lng},
          zoom: 8
        });
        this.result.forEach((item: any) => {
        	let point={lat:item.lat,lng:item.lng};
				this.createMarker(point);
			});
      }

   createMarker(point:any){
   	let marker = new google.maps.Marker({

        map: this.map,
        //animation: google.maps.Animation.DROP,
        position: point,
        optimized:false,
        //icon: icon,
        // shadow: icon.shadow,

    });
   }
}

@Component({
  selector: 'map-input-cords',
  templateUrl: 'view/calendar/addMarkerMap.component.html',
  //styleUrls: ['app.component.css'],
})
export class AddMarkerMapComponent {
  map:any;
  marker:any;
  title: string = 'My first angular2-google-maps project';
  lat: number = 42.605650;
  lng: number = 23.032772;
  ngOnInit(){
  	this.initMap();
  }
  initMap() {
        this.map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: this.lat, lng: this.lng},
            mapTypeId: 'roadmap',
            zoom:8	        
        });
        
        google.maps.event.addListener(this.map, 'click',(event:any)=> {
   		this.placeMarker(event.latLng);
   		console.log(event);
		});
      }

	 placeMarker(location:any) {
	 	//Determine the location where the user has clicked.
        document.getElementById('lat').value=location.lat();
        document.getElementById('lng').value=location.lng();
        console.log(location.lat());
        if(this.marker)
        this.marker.setVisible(false);
            

	    this.marker = new google.maps.Marker({
	        position: location, 
	        map: this.map
	    });
	}
	
}

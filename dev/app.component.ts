import { Component, OnInit } from '@angular/core';

import './rxjs-operators';
@Component({
  selector: 'my-app',
  template: `<router-outlet></router-outlet>`,
  styleUrls: ['css/week.component.css','css/login.component.css']
  
})
export class AppComponent implements OnInit{ 
	ngOnInit(){
		//google button 
		let meta = document.createElement('meta');
        meta.name = 'google-signin-client_id';
        meta.content = '964204965770-k01hrq2dpmi18ou5lq1ca6b7ktfa70gu.apps.googleusercontent.com';
        document.getElementsByTagName('head')[0].appendChild(meta);
        let node = document.createElement('script');
        node.src = 'https://apis.google.com/js/platform.js?onload=onLoadGoogleAPI';
        node.type = 'text/javascript';
        document.getElementsByTagName('body')[0].appendChild(node);
	}
}

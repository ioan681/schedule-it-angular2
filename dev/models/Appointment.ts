export class Appointment {
	id:number;
	name:string;
	user_id:number;
	color:string;
	date:string;
	start:string;
	end:string;
	lat:string;
	lng:string;
}
export class User {
  id: number;
  name: string;
  email: string;
  telephone: string;
  profession: string;
  about: string;
  img_name: string;
  is_logged: boolean;
}